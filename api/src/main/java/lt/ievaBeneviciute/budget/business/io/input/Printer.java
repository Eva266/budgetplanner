package lt.ievaBeneviciute.budget.business.io.input;

import lt.ievaBeneviciute.budget.domain.Cheque;

public interface Printer {

    String print(Cheque cheque);

}
