package lt.ievaBeneviciute.budget.domain;

public interface Product {

    String getName();

    Double getPrice();

}