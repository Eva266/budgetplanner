package lt.ievaBeneviciute.budget.domain;

import java.util.List;

public interface Cheque {

    List<Product> getProducts();

    void addProduct(Product product);

}
