package lt.ievaBeneviciute.budget.domain;

import java.util.ArrayList;
import java.util.List;

public class ChequeImpl implements Cheque{

    private List<Product> productList = new ArrayList<Product>();

    public ChequeImpl() {

    }

    public ChequeImpl(Product product) {
        productList.add(product);
    }

    public List<Product> getProducts() {
        return productList;
    }

    public void addProduct(Product product) {
        productList.add(product);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChequeImpl cheque = (ChequeImpl) o;

        return productList != null ? productList.equals(cheque.productList) : cheque.productList == null;
    }

    @Override
    public int hashCode() {
        return productList != null ? productList.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ChequeImpl{" +
                "productList=" + productList +
                '}';
    }
}
