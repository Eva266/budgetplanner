package lt.ievaBeneviciute.budget.domain;

public class ProductImpl implements Product {

    private String name;
    private Double price;

    public ProductImpl(String productName, Double productPrice) {
        name = productName;
        price = productPrice;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductImpl product = (ProductImpl) o;

        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        return price != null ? price.equals(product.price) : product.price == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProductImpl{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
