package lt.ievaBeneviciute.budget.domain;

import org.junit.Assert;
import org.junit.Test;

public class ProductImplTest {

    public Product testableProduct = new ProductImpl("Cappy“ Apelsinų sultys", new Double(1.59));

    @Test
    public void testProductName() {
        Assert.assertEquals("Cappy“ Apelsinų sultys", testableProduct.getName());
    }

    @Test
    public void testProductPrice() {
        Assert.assertEquals(new Double(1.59), testableProduct.getPrice());
    }

    @Test
    public void testProductEqual() {
        Assert.assertEquals(testableProduct, new ProductImpl("Cappy“ Apelsinų sultys", new Double(1.59)));
    }

}