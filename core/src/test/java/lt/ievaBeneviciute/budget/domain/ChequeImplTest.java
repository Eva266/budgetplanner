package lt.ievaBeneviciute.budget.domain;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ChequeImplTest {

    private Cheque testableCheque = new ChequeImpl();

    @Test
    public void testGetProducts() throws Exception {
        Assert.assertTrue(testableCheque.getProducts().isEmpty());
    }

    @Test
    public void testAddProduct() throws Exception {
        testableCheque.addProduct(new ProductImpl("juice", 1.0));
        Assert.assertEquals(1, testableCheque.getProducts().size());
    }

    @Test
    public void testChequesAreEqual() throws Exception {
        testableCheque.addProduct(new ProductImpl("juice", 1.0));
        Assert.assertEquals(testableCheque, new ChequeImpl(new ProductImpl("juice", 1.0)));
    }

}